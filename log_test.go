package logs

import (
	"errors"
	"fmt"
	"testing"
)

func TestLog(t *testing.T) {
	var temp = []int{3, 1, 2}
	out := &OutputFile{Path: "./log.out"}
	v := New(out)
	v.fullTraceLog = false
	v.data = fmt.Sprint(temp)
	v.Log()
}

func TestHandleError(t *testing.T) {
	out := &OutputFile{Path: "./log.out"}
	v := New(out)
	defer v.HandlerError()
	v.fullTraceLog = false
	v.depthStackTrace = 2
	v.data = fmt.Sprint(v)
	panic(errors.New("boom"))
}
