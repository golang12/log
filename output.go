package logs

import (
	"os"
)

type OutputFile struct {
	Path   string
	output *os.File
}

func (out *OutputFile) Write(p []byte) (n int, err error) {
	return out.output.Write(p)
}

func (out *OutputFile) WriteString(s string) (n int, err error) {
	return out.output.WriteString(s)
}

func (out *OutputFile) Close() error {
	return out.output.Close()
}

func (out *OutputFile) Open() error {
	if _, err := os.Stat(out.Path); err != nil {
		if os.IsNotExist(err) {
			out.output, err = os.Create(out.Path)
			if err != nil {
				return err
			}
		} else {
			return err
		}
	} else {
		out.output, err = os.OpenFile(out.Path, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
		if err != nil {
			return err
		}
	}
	return nil
}

type OutputConsole struct {}

func (out *OutputConsole) Write(p []byte) (n int, err error) {
	return os.Stdout.Write(p)
}

func (out *OutputConsole) WriteString(s string) (n int, err error) {
	return os.Stdout.WriteString(s)
}

func (out *OutputConsole) Close() error {
	return nil
}

func (out *OutputConsole) Open() error {
	return nil
}
