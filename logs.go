package logs

import (
	"fmt"
	"io"
	"runtime"
	"time"
)

type Log struct {
	*log
}

type log struct {
	output          Output
	fullTraceLog    bool
	depthStackTrace int
	tag             string
	data            string
}

type Output interface {
	io.WriteCloser
	io.StringWriter
	Open() error
}

func New(output Output) Log {
	return Log{
		&log{
			fullTraceLog:    true,
			tag:             "INFO",
			data:            "",
			output:          output,
			depthStackTrace: 1,
		},
	}
}

func (l *Log) SetTag(tag string) {
	l.tag = tag
}

func (l *Log) SetData(data string) {
	l.data = data
}

func (l *Log) SetDepth(depth int) {
	l.depthStackTrace = depth
}

func (l *Log) SetFullTrace(flag bool) {
	l.fullTraceLog = flag
}

func (l *Log) Write(p []byte) (n int, err error) {
	return l.output.Write(p)
}

func (l *Log) WriteString(s string) (n int, err error) {
	return l.output.WriteString(s)
}

func (l Log) Log() {
	if l.log == nil {
		panic("Log is not init")
	} else {
		err := l.output.Open()
		if err != nil {
			panic(err)
		}

		_, err = l.WriteString(timeMark(l.tag) + "\n")
		if err != nil {
			panic(err)
		}
		if l.fullTraceLog {
			var buf [4096]byte
			n := runtime.Stack(buf[:], false)
			_, err = l.WriteString("StackTrace:\n")
			if err != nil {
				panic(err)
			}
			_, err = l.Write(buf[:n])
			if err != nil {
				panic(err)
			}
		} else {
			_, file, line, _ := runtime.Caller(l.depthStackTrace)
			str := file + " " + fmt.Sprint(line)
			_, err = l.WriteString("StackTrace: " + str + "\n")
			if err != nil {
				panic(err)
			}
		}
		_, err = l.WriteString("Data: " + l.data + "\n\n")
		if err != nil {
			panic(err)
		}
		err = l.output.Close()
		if err != nil {
			panic(err)
		}
	}
}

func (l Log) HandlerError() {
	if l.log == nil {
		panic("Log is not init")
	} else {
		err := l.output.Open()
		if err != nil {
			panic(err)
		}
		if r := recover(); r != nil {
			_, err = l.WriteString(timeMark("ERROR") + "\n")
			if err != nil {
				panic(err)
			}
			if r, ok := r.(error); ok {
				_, err = l.WriteString("Error: " + fmt.Sprintf("%#v\n", r.Error()))
			} else {
				_, err = l.WriteString("Error: " + fmt.Sprintf("%#v\n", r))
			}
			if err != nil {
				panic(err)
			}
			_, err = l.WriteString("Data: " + l.data)
			if err != nil {
				panic(err)
			}
			if l.fullTraceLog {
				var buf [4096]byte
				n := runtime.Stack(buf[:], false)
				_, err = l.WriteString("StackTrace:\n")
				if err != nil {
					panic(err)
				}
				_, err = l.Write(buf[:n])
				if err != nil {
					panic(err)
				}
				_, err = l.WriteString("\n\n")
				if err != nil {
					panic(err)
				}
			} else {
				_, file, line, _ := runtime.Caller(l.depthStackTrace)
				str := file + " " + fmt.Sprint(line)
				_, err = l.WriteString("StackTrace: " + str + "\n")
				if err != nil {
					panic(err)
				}
			}
		}
		err = l.output.Close()
		if err != nil {
			panic(err)
		}
	}
}

func timeMark(tag string) string {
	t := time.Now().Format("[2006-01-02 15:04:05]")
	return t + "[" + tag + "] "
}
